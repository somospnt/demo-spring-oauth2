package com.somospnt.demo.jwt.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoJwtServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoJwtServerApplication.class, args);
	}
}
