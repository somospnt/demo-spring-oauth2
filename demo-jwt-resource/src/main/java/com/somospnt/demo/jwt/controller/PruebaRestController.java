package com.somospnt.demo.jwt.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PruebaRestController {
    
    @PreAuthorize("hasAuthority('APP_READ')")
    @RequestMapping(path = "/app", method = RequestMethod.GET)
    public String readApp() {
        return "Hola Mundo desde la lectura de APP_READ";
    }
    
    @PreAuthorize("hasAuthority('APP_WRITE')")
    @RequestMapping(path = "/app", method = RequestMethod.POST)
    public String writeApp() {
        return "Hola Mundo desde la escritura de APP_WRITE";
    }
    
}
